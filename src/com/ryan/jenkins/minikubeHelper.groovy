package com.ryan.jenkins

def mapMinikubeDockerEnv() {
    String dockerEnvOutput = sh (
        script: 'minikube docker-env',
        returnStdout: true
    ).trim()

    // Nice groovy loops like 'eachLine' don't play nicely with Jenkins, so we'll loop the old fashioned way
    String[] dockerEnvOutputLines = dockerEnvOutput.split('\\r?\\n')
    for (int i = 0; i < dockerEnvOutputLines.length; i++) {
        if (dockerEnvOutputLines[i] =~ /SET/) {
            String key = dockerEnvOutputLines[i].replace("SET", "").split("=")[0].trim()
            String value = dockerEnvOutputLines[i].replace("SET", "").split("=")[1].trim()
            echo "DockerEnv[${key}, ${value}]"
            env."${key}" = value
        }
    }
}