package com.ryan.jenkins

@Grab('org.yaml:snakeyaml')

import org.yaml.snakeyaml.Yaml

def runPipeline() {

    def version = "0.1.${BUILD_NUMBER}-SNAPSHOT"

    node {

        stage('Checkout repository') {
            checkout scm
        }

        Map pipeYaml
        String yamlLocation = pwd() + '/CICD/pipeline.yml'
        pipeYaml = (Map) new Yaml().load(new File(yamlLocation).text)

        // Determine and execute pipeline based on the type
        println('About to enter switch')
        switch (pipeYaml.pipeline.type) {
            case 'microservice':
                println('Microservice pipe selected')
                new microservicePipeline().runPipeline(pipeYaml, version)
                break
            case 'module':
                println('Module pipe selected')
                new modulePipeline().runPipeline(pipeYaml, version)
                break
            default:
                break
        }

    }

    return this

}