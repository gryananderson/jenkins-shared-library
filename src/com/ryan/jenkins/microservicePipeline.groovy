package com.ryan.jenkins

def runPipeline(Map parameters, String version) {

    stage("Compile") {
        sh "./gradlew clean build -DinputVersion=${version}"
    }

    stage('Sonarqube') {
        def scannerHome = tool name: 'SonarqubeScanner'
        println(scannerHome)

        withSonarQubeEnv('Sonarqube') {
            sh "${scannerHome}/bin/sonar-scanner"
        }
        timeout(time: 5, unit: 'MINUTES') {
            waitForQualityGate abortPipeline: true
        }
    }

    stage('Build image') {
        sh "docker build -t us.gcr.io/ryan-test-project-200518/${parameters.pipeline.name}:latest -f ./CICD/Dockerfile ."
        sh "docker tag us.gcr.io/ryan-test-project-200518/${parameters.pipeline.name}:latest us.gcr.io/ryan-test-project-200518/${parameters.pipeline.name}:${version}"
    }

    stage('Push image to GCR') {
        docker.withRegistry('https://us.gcr.io', 'gcr:ryan-test-project-200518') {
            sh "docker push us.gcr.io/ryan-test-project-200518/${parameters.pipeline.name}:${version}"
        }
    }

    stage('Deploy to Kubernetes') {
        sh 'echo $USER'
        sh "kubectl apply -f ./CICD/kubernetes/${parameters.pipeline.name}-deployment.yml"
        sh "kubectl apply -f ./CICD/kubernetes/${parameters.pipeline.name}-service.yml"
        sh "kubectl set image -n gaming-api-dev deployment/${parameters.pipeline.name} ${parameters.pipeline.name}=us.gcr.io/ryan-test-project-200518/${parameters.pipeline.name}:${version}"
    }

}
