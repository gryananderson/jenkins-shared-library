package com.ryan.jenkins

def runPipeline(Map parameters, String version) {

    stage("Compile") {
        sh "./gradlew clean build -PinputVersion=${version}"
    }

    stage('Upload to artifactory') {
        def server = Artifactory.server('artifactory')
        def rtGradle = Artifactory.newGradleBuild()
        def buildInfo = Artifactory.newBuildInfo()

        rtGradle.useWrapper = true
        rtGradle.deployer repo: 'gradle-dev-local', server: server
        rtGradle.deployer.artifactDeploymentPatterns.addInclude('*.jar')
        rtGradle.deployer.artifactDeploymentPatterns.addInclude('*.pom')
        rtGradle.deployer.deployArtifacts = true
        rtGradle.deployer.deployMavenDescriptors = true
        rtGradle.deployer.deployIvyDescriptors = true

        rtGradle.run buildFile: 'build.gradle', tasks: 'clean artifactoryPublish -PinputVersion=' + version, buildInfo: buildInfo
        rtGradle.deployer.deployArtifacts buildInfo
        server.publishBuildInfo buildInfo
    }

}
