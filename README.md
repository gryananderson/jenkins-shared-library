This is a sandbox repo I've been working on with regards to Jenkins shared libraries.

First, set up the Jenkins shared library with the Jenkins management console.

Once it's configured, each code repository (e.g. microservices, modules) will need the following as their Jenkinsfile:
```
@Library("shared-pipeline@master")
import com.ryan.jenkins.*

def pipeline = new pipeline().runPipeline()

```

In addition, each code repository will need a file `pipeline.yml` which is in the following format:
```
pipeline:
  type: microservice
  name: test-service
```

Or for modules:
```
pipeline:
  type: module
  name: test-module
```